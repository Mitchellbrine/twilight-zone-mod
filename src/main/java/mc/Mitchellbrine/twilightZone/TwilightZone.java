package mc.Mitchellbrine.twilightZone;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;

import mc.Mitchellbrine.twilightZone.common.CommonProxy;
import mc.Mitchellbrine.twilightZone.common.block.DoorLocked;
import mc.Mitchellbrine.twilightZone.common.block.TwilightZoneDoor;
import mc.Mitchellbrine.twilightZone.common.block.VoidOfReality;
import mc.Mitchellbrine.twilightZone.common.dimension.DimensionBlocks;
import mc.Mitchellbrine.twilightZone.common.dimension.TZBiome;
import mc.Mitchellbrine.twilightZone.common.dimension.TZWorldProvider;
import mc.Mitchellbrine.twilightZone.common.entity.EntityAlicia;
import mc.Mitchellbrine.twilightZone.common.entity.EntityArchie;
import mc.Mitchellbrine.twilightZone.common.event.FatePotion;
import mc.Mitchellbrine.twilightZone.common.event.ImmortalityEvent;
import mc.Mitchellbrine.twilightZone.common.event.JoinChecker;
import mc.Mitchellbrine.twilightZone.common.item.GregorysRecorder;
import mc.Mitchellbrine.twilightZone.common.item.ImmortalContract;
import mc.Mitchellbrine.twilightZone.common.item.ItemPotionBow;
import mc.Mitchellbrine.twilightZone.common.item.ItemTZFood;
import mc.Mitchellbrine.twilightZone.common.item.KeyOfImagination;
import mc.Mitchellbrine.twilightZone.common.item.RecorderTape;
import mc.Mitchellbrine.twilightZone.common.item.TZPotion;
import mc.Mitchellbrine.twilightZone.common.tileentity.TileEntityDoor;
import mc.Mitchellbrine.twilightZone.common.tileentity.TileEntityDoorLocked;
import mc.Mitchellbrine.twilightZone.util.AutoUpdateChecker;
import mc.Mitchellbrine.twilightZone.util.ConfigUtil;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.stats.Achievement;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.biome.BiomeGenBase.Height;
import net.minecraftforge.common.AchievementPage;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid=Constants.MODID, name = "The Twilight Zone Mod", version = Constants.VERSION, useMetadata = true)
public class TwilightZone
{
	@SidedProxy(clientSide = Constants.CLIENTPROXY, serverSide = Constants.COMMONPROXY)
	public static CommonProxy proxy;

	public static TwilightTab twilightTab = new TwilightTab(CreativeTabs.getNextID(),"twilightZone");

	public static int twilightDimensionID;

	public static BiomeGenBase theTwilightZone;

	// Blocks

	public static Block twilightZoneDoor;
	public static Block doorLocked;

	public static Block voidOfReality;

	// Items

	public static Item keyOfImagination;
	public static Item chocolateSoda;
	public static Item immortalityContract;
	public static Item fateBow;
	public static Item fateBottle;
	public static Item gregorysRecorder;
	public static Item recorderTape;

	// Potion

	public static Potion fatePotion;

	// Configuration

	public static Configuration config;
	public static int doorChanceInterval;
	public static boolean displayEpisodes;
	public static boolean notifyMe;
	public static String playerBlacklist;
	public static String creationBlacklist;
	
	public static int archieDistanceTrack;
	public static boolean archieStealing;
	public static int archieDistanceSteal;

	public static boolean updateAvailable;

	public static boolean originalColors;

	// Achievements

	public static Achievement twilightZone;
	public static Achievement keyAch;

	public static AchievementPage twilightPage;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		// Configuration

		proxy.registerThings();

		config = new Configuration(event.getSuggestedConfigurationFile());

		String generalC = Configuration.CATEGORY_GENERAL;
		
		config.load();

		Property archieTrack = ConfigUtil.createPropertyInt(config, generalC, "Arch Hammer Tracking Distance", 15, "This is the distance Arch will be before shape shifting");
		TwilightZone.archieDistanceTrack = archieTrack.getInt();
		
		Property archieStealB = ConfigUtil.createPropertyBoolean(config, generalC, "Does Arch Hammer steal?", true, "This turns on and off Arch stealing your stuff when close!");
		TwilightZone.archieStealing = archieStealB.getBoolean(true);
		
		Property archieStealI = ConfigUtil.createPropertyInt(config, generalC, "Arch Hammer Armor Steal Distance", 10, "This is the distance Arch will be before stealing your stuff!");
		TwilightZone.archieDistanceSteal = archieStealI.getInt();
		
		Property doorChanceIntervalP = ConfigUtil.createPropertyInt(config, generalC, "Episode Interval", 12000, "This is the maximum time between items spawning (in ticks)");
		TwilightZone.doorChanceInterval = doorChanceIntervalP.getInt();
		
		Property displayEpisodesP = ConfigUtil.createPropertyBoolean(config, generalC, "Display Episode Numbers?", false, "Do you want to show the episodes certain items come from?");
		TwilightZone.displayEpisodes = displayEpisodesP.getBoolean(false);
		
		Property notifyMeP = ConfigUtil.createPropertyBoolean(config, generalC, "Notify Me On Update?", false, "This is for the auto-update detecter. (Do not use! It is buggy and I haven't been working on it)!");
		TwilightZone.notifyMe = notifyMeP.getBoolean(false);
		
		Property playerBlacklistP = ConfigUtil.createPropertyString(config, generalC, "Shape Shifter Blacklist", "<Insert Player Name Here>", "Here you can remove players that Arch Hammer can be named!");
		TwilightZone.playerBlacklist = playerBlacklistP.getString();
		
		Property creationBlacklistP = ConfigUtil.createPropertyString(config, generalC, "Tape Recorder Blacklist", "ingot_gold, diamond, ingot_iron, gold_block, diamond_block, iron_block, emerald, emerald_block", "Here you can remove items/blocks that can be spawned in by the Tape Recorder!");
		TwilightZone.creationBlacklist = creationBlacklistP.getString();

		Property originalColorsP = ConfigUtil.createPropertyBoolean(config, "Dimension", "Use Original Colors?", false, "Here you can enable or disable the original shading of the Twilight Zone!");
		TwilightZone.originalColors = originalColorsP.getBoolean(false);
		
		Property twilightDimensionIDP = ConfigUtil.createPropertyInt(config, "Dimension", "Twilight Zone Dimension ID", 156);
		TwilightZone.twilightDimensionID = twilightDimensionIDP.getInt();
				
		config.save();

		registerPotionStuff();

		MinecraftForge.EVENT_BUS.register(new FatePotion());

		fatePotion = (new TZPotion(24, false, 0)).setIconIndex(0, 0).setPotionName("potion.fatePotion");

		// Blocks

		twilightZoneDoor = new TwilightZoneDoor().setBlockName("theDoor").setBlockTextureName("twilightZone:doorIcon");
		voidOfReality = new VoidOfReality().setBlockUnbreakable().setCreativeTab(twilightTab).setBlockName("realityVoid").setBlockTextureName("twilightZone:realityVoid");
		doorLocked = new DoorLocked().setCreativeTab(twilightTab).setBlockName("theDoor").setBlockTextureName("twilightZone:doorIcon");

		// Items

		keyOfImagination = new KeyOfImagination().setFull3D().setMaxStackSize(1).setCreativeTab(twilightTab).setUnlocalizedName("keyOfImagination").setTextureName("twilightZone:keyOfImagination");
		chocolateSoda = new ItemFood(10, 0.8F, false).setCreativeTab(twilightTab).setMaxStackSize(1).setUnlocalizedName("chocolateSoda").setTextureName("twilightZone:chocolateSoda");
		immortalityContract = new ImmortalContract().setFull3D().setMaxStackSize(1).setCreativeTab(twilightTab).setUnlocalizedName("immortalityContract").setTextureName("twilightZone:immortalContract");
		fateBow = new ItemPotionBow().setUnlocalizedName("fateBow").setTextureName("bow_standby");
		fateBottle = new ItemTZFood(0, 0.3F, false).setAlwaysEdible().setPotionEffect(TwilightZone.fatePotion.id, 10, 0, 1.0F).setCreativeTab(twilightTab).setUnlocalizedName("fatePotion").setTextureName("twilightZone:fatePotion");
		gregorysRecorder = new GregorysRecorder().setCreativeTab(twilightTab).setUnlocalizedName("tapeRecorder").setTextureName("twilightZone:tapeRecorder");
		recorderTape = new RecorderTape().setCreativeTab(twilightTab).setUnlocalizedName("recorderTape").setTextureName("twilightZone:recorderTape");

		// Registering Blocks and Items

		GameRegistry.registerBlock(twilightZoneDoor, "theDoor_unlocked");
		GameRegistry.registerBlock(voidOfReality, "realityVoid");
		GameRegistry.registerBlock(doorLocked, "theDoor");

		GameRegistry.registerItem(keyOfImagination, "key_of_imagination");
		GameRegistry.registerItem(chocolateSoda, "chocolate_soda");
		GameRegistry.registerItem(immortalityContract, "immortality_contract");
		GameRegistry.registerItem(fateBow, "fate_bow");
		GameRegistry.registerItem(fateBottle, "fate_potion");
		GameRegistry.registerItem(gregorysRecorder, "tape_recorder");
		GameRegistry.registerItem(recorderTape, "recorder_tape");

		DimensionBlocks.init();

		// Registering TileEntities

		GameRegistry.registerTileEntity(TileEntityDoor.class, "theDoorTE");
		GameRegistry.registerTileEntity(TileEntityDoorLocked.class, "theDoorLockedTE");

		// Registering Achievments

		twilightZone = new Achievement("tZone", "tZone", 0, 0, twilightZoneDoor, (Achievement)null).initIndependentStat().registerStat();
		keyAch = new Achievement("keyAch", "keyAch", 2, 0, keyOfImagination, twilightZone).registerStat();

		twilightPage = new AchievementPage("The Twilight Zone Mod", new Achievement[]{twilightZone,keyAch});

		AchievementPage.registerAchievementPage(twilightPage);

		// Entity Registetring

		EntityRegistry.registerModEntity(EntityAlicia.class, "alicia", 300, this, 80, 3, true);
		EntityRegistry.registerModEntity(EntityArchie.class, "archie", 301, this, 80, 3, true);

		registerEntityEgg(EntityAlicia.class, 44975, 12623485);
		registerEntityEgg(EntityArchie.class, 44975, 12623485);

		theTwilightZone = new TZBiome(165).setColor(747097).setBiomeName("The Twilight Zone").setHeight(new Height(0.2F, 0.5F));

		DimensionManager.registerProviderType(twilightDimensionID, TZWorldProvider.class, true);
		DimensionManager.registerDimension(twilightDimensionID, twilightDimensionID);
	}

	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		// Event Registry

		MinecraftForge.EVENT_BUS.register(new JoinChecker());
		MinecraftForge.EVENT_BUS.register(new ImmortalityEvent());

		// Recipe Registry

		GameRegistry.addRecipe(new ItemStack(keyOfImagination, 1), "XYX","BAC","DZD", Character.valueOf('X'), Items.porkchop, Character.valueOf('Y'), Blocks.grass, Character.valueOf('Z'), Items.ender_eye, Character.valueOf('A'), Items.iron_ingot, Character.valueOf('B'), Blocks.netherrack, Character.valueOf('C'), Blocks.end_stone, Character.valueOf('D'), new ItemStack(Items.skull, 1, 1));
		GameRegistry.addRecipe(new ItemStack(twilightZoneDoor, 1),"XYX","YZY","XYX", Character.valueOf('X'), Blocks.obsidian, Character.valueOf('Y'), Blocks.end_stone, Character.valueOf('Z'), Items.iron_door);
		GameRegistry.addRecipe(new ItemStack(recorderTape, 1), "XXX","YYY","ZZZ",Character.valueOf('X'),Items.paper, Character.valueOf('Y'), Items.string, Character.valueOf('Z'), Blocks.redstone_block);

		try
		{
			AutoUpdateChecker.isUpdateAvailable();
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}


	}

	public static int getUniqueEntityId()
	{
		int startEntityId = 300;
		do
		{
			++startEntityId ;
		}
		while (EntityList.getStringFromID(startEntityId) != null);

		return startEntityId;
	}

	@SuppressWarnings("unchecked")
	public static void registerEntityEgg(Class <? extends Entity > entity, int primaryColor, int secondaryColor)
	{
		int id = getUniqueEntityId();
		EntityList.IDtoClassMapping.put(Integer.valueOf(id), entity);
		EntityList.entityEggs.put(Integer.valueOf(id), new EntityList.EntityEggInfo(id, primaryColor, secondaryColor));
	}

	public static void registerPotionStuff()
	{
		Potion[] potionTypes = null;
		Field[] potionFields = Potion.class.getDeclaredFields();
		int amountOfPotionFields = potionFields.length;

		for (int currentField = 0; currentField < amountOfPotionFields; ++currentField)
		{
			Field currentFeild = potionFields[currentField];
			currentFeild.setAccessible(true);

			try
			{
				if (currentFeild.getName().equals("potionTypes") || currentFeild.getName().equals("potionTypes"))
				{
					Field error = Field.class.getDeclaredField("modifiers");
					error.setAccessible(true);
					error.setInt(currentFeild, currentFeild.getModifiers() & -17);
					potionTypes = (Potion[])((Potion[])currentFeild.get((Object)null));
					Potion[] newPotionTypes = new Potion[256];
					System.arraycopy(potionTypes, 0, newPotionTypes, 0, potionTypes.length);
					currentFeild.set((Object)null, newPotionTypes);
				}
			}
			catch (Exception exception)
			{
				System.err.println("Severe error, please report this to the mod author! Crash Log:");
				System.err.println(exception);
			}
		}
	}


}