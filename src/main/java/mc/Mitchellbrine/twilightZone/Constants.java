package mc.Mitchellbrine.twilightZone;

public class Constants {

public static final String MODID = "TwilightZone";
public static final String VERSION = "0.0.1";

public static final String CLIENTPROXY = "mc.Mitchellbrine.twilightZone.client.ClientProxy";
public static final String COMMONPROXY = "mc.Mitchellbrine.twilightZone.common.CommonProxy";
	
}
