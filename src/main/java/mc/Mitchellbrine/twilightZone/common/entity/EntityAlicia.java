package mc.Mitchellbrine.twilightZone.common.entity;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIMoveTowardsTarget;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class EntityAlicia extends EntityCreature{

	public EntityAlicia(World par1World) {
		super(par1World);
        this.tasks.addTask(8, new EntityAIWatchClosest(this, EntityPlayer.class, 10.0F));
        this.tasks.addTask(1, new EntityAIMoveTowardsTarget(this, 0.3D, 10.0F));
	}

	@Override
	protected void entityInit() {
		super.entityInit();
	}
	
	@Override
    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(20.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.5D);
    }

	@Override
	public void readEntityFromNBT(NBTTagCompound var1) {
		
	}
	
    /**
     * Returns the sound this mob makes when it is hurt.
     */
    protected String getHurtSound()
    {
        return "mob.irongolem.hit";
    }

    /**
     * Returns the sound this mob makes on death.
     */
    protected String getDeathSound()
    {
        return "mob.irongolem.death";
    }
    
    @Override
    protected String getLivingSound() {
    	return "mob.villager.idle";
    }

	@Override
	public void writeEntityToNBT(NBTTagCompound var1) {
		
	}


}
