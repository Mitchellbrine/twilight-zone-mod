package mc.Mitchellbrine.twilightZone.common.entity;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIMoveTowardsTarget;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class EntityArchie extends EntityMob{

	public String playerName;
	
	public static String MHFArchie = "Blaze, CaveSpider, Chicken, Cow, Enderman, Ghast, Golem, LavaSlime, MushroomCow, Ocelot, Pig, PizZombie, Sheep, Slime, Spider, Squid, Villager, Cactus, Chest, Melon, OakLog, Pumpkin, TNT, TNT2, ArrowUp, ArrowDown, ArrowLeft, ArrowRight, Exclamation, Question";
	public static String vanillaArchie = "Creeper, WitherSkeleton, Skeleton, Zombie,";
	
	public EntityArchie(World par1World) {
		super(par1World);
        this.tasks.addTask(8, new EntityAIWatchClosest(this, EntityPlayer.class, 10.0F));
        this.tasks.addTask(1, new EntityAIMoveTowardsTarget(this, 0.3D, 10.0F));
	}

	@Override
	protected void entityInit() {
		super.entityInit();
	}
	
	@Override
    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(20.0D);
        if (this.isSprinting()) {
        	this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.75D);        	
        }
        else {
        	this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.5D);
        }
    }

	@Override
	public void readEntityFromNBT(NBTTagCompound var1) {
		super.readEntityFromNBT(var1);
		if (playerName != "") {
		playerName = var1.getString("player");
		}
		else {
			playerName = "";
		}
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound var1) {
		super.writeEntityToNBT(var1);
		if (playerName != null) {
		var1.setString("player", playerName);
		}
	}
	
	
	public String getName() {
		return this.playerName;
	}
	
	@Override
	public void onEntityUpdate() {
		super.onEntityUpdate();
		if (this.getCustomNameTag() != "" || !TwilightZone.playerBlacklist.contains(this.getCustomNameTag())) {
			playerName = this.getCustomNameTag();
		}
		else if (this.getCustomNameTag() == "" || TwilightZone.playerBlacklist.contains(this.getCustomNameTag())){
			if (this.worldObj.getClosestVulnerablePlayerToEntity(this, TwilightZone.archieDistanceTrack) != null) {
				this.setSprinting(true);
				playerName = this.worldObj.getClosestVulnerablePlayerToEntity(this, TwilightZone.archieDistanceTrack).getCommandSenderName();
			}
			else {
				playerName = "";
			}
		}
		this.calculateArmor();
	}
	
	public boolean isArchieMHF() {
		if (MHFArchie.contains(playerName)) {
			return true;
		}
		return false;
	}
	
	public boolean isArchieVanilla() {
		if (vanillaArchie.contains(playerName)) {
			return true;
		}
		return false;
	}
	
	@Override
	protected Item getDropItem() {
		if (playerName.equals("Notch")) {
			return Items.apple;
		}
		return Items.rotten_flesh;
	}
	
	private void calculateArmor() {
		if (TwilightZone.archieStealing == true) {
		if (this.worldObj.getClosestVulnerablePlayerToEntity(this, TwilightZone.archieDistanceSteal) != null) {
			this.setCurrentItemOrArmor(0, this.worldObj.getClosestVulnerablePlayerToEntity(this, TwilightZone.archieDistanceSteal).getEquipmentInSlot(0));
			this.setCurrentItemOrArmor(1, this.worldObj.getClosestVulnerablePlayerToEntity(this, TwilightZone.archieDistanceSteal).getEquipmentInSlot(1));
			this.setCurrentItemOrArmor(2, this.worldObj.getClosestVulnerablePlayerToEntity(this, TwilightZone.archieDistanceSteal).getEquipmentInSlot(2));
			this.setCurrentItemOrArmor(3, this.worldObj.getClosestVulnerablePlayerToEntity(this, TwilightZone.archieDistanceSteal).getEquipmentInSlot(3));
			this.setCurrentItemOrArmor(4, this.worldObj.getClosestVulnerablePlayerToEntity(this, TwilightZone.archieDistanceSteal).getEquipmentInSlot(4));
		}
		else {
			this.setCurrentItemOrArmor(0, null);
			this.setCurrentItemOrArmor(1, null);
			this.setCurrentItemOrArmor(2, null);
			this.setCurrentItemOrArmor(3, null);
			this.setCurrentItemOrArmor(4, null);
			
		}
		}
		else {
			this.setCurrentItemOrArmor(0, null);
			this.setCurrentItemOrArmor(1, null);
			this.setCurrentItemOrArmor(2, null);
			this.setCurrentItemOrArmor(3, null);
			this.setCurrentItemOrArmor(4, null);			
		}
	}
	
}
