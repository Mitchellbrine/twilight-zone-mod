package mc.Mitchellbrine.twilightZone.common.tileentity;

import java.util.Random;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import mc.Mitchellbrine.twilightZone.common.entity.EntityAlicia;
import mc.Mitchellbrine.twilightZone.common.entity.EntityArchie;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;

public class TileEntityDoor extends TileEntity
{
	public int doorEntities = 1;

	public TileEntityDoor() 
	{
		this.canUpdate();
	}

	@Override
	public void updateEntity()
	{
		Random random = new Random();
		if (random.nextInt(TwilightZone.doorChanceInterval) == 0) 
		{
			int randItem = random.nextInt(5);
			if (randItem == 0)
			{
				EntityItem soda = new EntityItem(worldObj, xCoord, yCoord, zCoord, new ItemStack(TwilightZone.chocolateSoda));
				soda.setEntityItemStack(new ItemStack(TwilightZone.chocolateSoda, 1));
				soda.setLocationAndAngles(xCoord, yCoord, zCoord, 0, 0);
				worldObj.spawnEntityInWorld(soda);
			}
			else if (randItem == 1) 
			{
				EntityItem contract = new EntityItem(worldObj, xCoord, yCoord, zCoord, new ItemStack(TwilightZone.immortalityContract));
				contract.setEntityItemStack(new ItemStack(TwilightZone.immortalityContract, 1));
				contract.setLocationAndAngles(xCoord, yCoord, zCoord, 0, 0);
				worldObj.spawnEntityInWorld(contract);				
			}
			else if (randItem == 2) 
			{
				EntityAlicia alicia = new EntityAlicia(worldObj);
				alicia.setLocationAndAngles(xCoord, yCoord, zCoord, 0, 0);
				worldObj.spawnEntityInWorld(alicia);
			}
			else if (randItem == 3) 
			{
				EntityItem potion = new EntityItem(worldObj, xCoord, yCoord, zCoord, new ItemStack(TwilightZone.fateBottle));
				potion.setEntityItemStack(new ItemStack(TwilightZone.fateBottle, 1));
				potion.setLocationAndAngles(xCoord, yCoord, zCoord, 0, 0);
				worldObj.spawnEntityInWorld(potion);				
			}
			else if (randItem == 4) 
			{
				EntityArchie archie = new EntityArchie(worldObj);
				archie.setLocationAndAngles(xCoord, yCoord, zCoord, 0, 0);
				worldObj.spawnEntityInWorld(archie);				
			}
			else if (randItem == 5)
			{
				EntityItem recorder = new EntityItem(worldObj, xCoord, yCoord, zCoord, new ItemStack(TwilightZone.gregorysRecorder));
				recorder.setEntityItemStack(new ItemStack(TwilightZone.gregorysRecorder));
				recorder.setLocationAndAngles(xCoord, yCoord, zCoord, 0, 0);
				worldObj.spawnEntityInWorld(recorder);
			}
		}
	}

}
