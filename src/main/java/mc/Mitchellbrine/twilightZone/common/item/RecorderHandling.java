package mc.Mitchellbrine.twilightZone.common.item;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class RecorderHandling {

	public static void calculateChatMessage(EntityPlayerMP player, String message, String username) {
		if (message.contains("appeared")) {
			if (message.charAt(0) == Character.valueOf('A') && message.charAt(1) == Character.valueOf(' ')) {
			if (player.inventory.hasItemStack(new ItemStack(TwilightZone.gregorysRecorder, 1, 1))) {
				String itemEncoded = message;
				String itemWOA = itemEncoded.replaceAll("A","");
				String itemWOApp = itemWOA.replaceAll("appeared", "");
				String itemName = itemWOApp.replaceAll(" ", "");

				computeItems(player, username, itemName);
				
			}
			}
			else if (message.contains("An")) {
				if (player.inventory.hasItemStack(new ItemStack(TwilightZone.gregorysRecorder, 1, 1))) {
					String itemEncoded = message;
					String itemWOA = itemEncoded.replaceAll("An","");
					String itemWOApp = itemWOA.replaceAll("appeared", "");
					String itemName = itemWOApp.replaceAll(" ", "");
					
					computeItems(player, username, itemName);
					
			}
			}
			else if (message.contains("Some")) {
				if (player.inventory.hasItemStack(new ItemStack(TwilightZone.gregorysRecorder, 1, 1))) {
					String itemEncoded = message;
					String itemWOA = itemEncoded.replaceAll("Some","");
					String itemWOApp = itemWOA.replaceAll("appeared", "");
					String itemName = itemWOApp.replaceAll(" ", "");
				
					computeItems(player, username, itemName);
					
					}					
			}
		}
	}
	
	public static boolean isItem(String itemName) {
		Item item = (Item) Item.itemRegistry.getObject(itemName);
		
		if (item != null) {
			return true;
		}
		return false;		
	}
	
	public static boolean isBlock(String itemName) {
		Block item = (Block) Block.blockRegistry.getObject(itemName);
		
		if (item != null) {
			return true;
		}
		return false;
		
	}
	
	public static void computeItems(EntityPlayer player, String username, String itemName) {
		Item item = (Item) Item.itemRegistry.getObject(itemName);
		Block block = (Block) Block.blockRegistry.getObject(itemName);
		
		if (isItem(itemName)) {
			ItemStack tapedItem = new ItemStack(item, 1);
			tapedItem.setStackDisplayName("(Recorded) " + item.getUnlocalizedName().substring(5).toUpperCase());
			
			if ((player.inventory.hasItem(TwilightZone.recorderTape) || player.capabilities.isCreativeMode) && !TwilightZone.creationBlacklist.contains(itemName)) {
					ItemStack tape = new ItemStack(TwilightZone.recorderTape, 1);
					if (!tape.hasTagCompound() || !tape.getTagCompound().hasKey("itemName")) {
					tape.stackTagCompound = new NBTTagCompound();
					player.inventory.consumeInventoryItem(tape.getItem());
					tape.stackTagCompound.setString("itemName", itemName);
					if (!player.capabilities.isCreativeMode) {
					player.inventory.addItemStackToInventory(tape);
					}
					player.inventory.addItemStackToInventory(tapedItem);
					}
					}
				else if ((!player.inventory.hasItem(TwilightZone.recorderTape) && !player.capabilities.isCreativeMode) || TwilightZone.creationBlacklist.contains(itemName)){							
					player.addChatMessage(new ChatComponentText("*Click*"));
					player.worldObj.playSoundAtEntity((EntityPlayer)player, "random.click", 1.0F, 1.0F);
					
					if (!MinecraftServer.getServer().getConfigurationManager().isPlayerOpped(username)) {
						player.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_RED + "The item you described has been blocked by your server "+ EnumChatFormatting.DARK_RED + "administrator!"));
					}
				
				}				

		}
			else if (isBlock(itemName)) {
				ItemStack tapedItem = new ItemStack(block, 1);
				tapedItem.setStackDisplayName("(Recorded) " + block.getUnlocalizedName().substring(5).toUpperCase());
				if ((player.inventory.hasItem(TwilightZone.recorderTape) || player.capabilities.isCreativeMode) && !TwilightZone.creationBlacklist.contains(itemName)) {
					ItemStack tape = new ItemStack(TwilightZone.recorderTape, 1);
					if (!tape.hasTagCompound()) {
					tape.stackTagCompound = new NBTTagCompound();
					player.inventory.consumeInventoryItem(TwilightZone.recorderTape);
					tape.stackTagCompound.setString("itemName", itemName);
					if (!player.capabilities.isCreativeMode) {
					player.inventory.addItemStackToInventory(tape);
					}
					player.inventory.addItemStackToInventory(tapedItem);
					}
					}
			else if ((!player.inventory.hasItem(TwilightZone.recorderTape) && !player.capabilities.isCreativeMode) || TwilightZone.creationBlacklist.contains(itemName)){							
				player.addChatMessage(new ChatComponentText("*Click*"));
				player.worldObj.playSoundAtEntity((EntityPlayer)player, "random.click", 1.0F, 1.0F);
				
				if (!MinecraftServer.getServer().getConfigurationManager().isPlayerOpped(username)) {
					player.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_RED + "The item you described has been blocked by your server "+ EnumChatFormatting.DARK_RED + "administrator!"));
				}
			
			}
			}
		else {
			player.addChatMessage(new ChatComponentText("*Click*"));
			player.worldObj.playSoundAtEntity((EntityPlayer)player, "random.click", 1.0F, 1.0F);						
		}
	}
	
	

}
