package mc.Mitchellbrine.twilightZone.common.item;

import java.util.List;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class RecorderTape extends Item 
{
	public RecorderTape() 
	{
		this.setMaxStackSize(1);
	}

	@Override
	public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int par7, float par8, float par9, float par10)
	{
		if (player.isSneaking())
		{
			if (stack.stackTagCompound != null) 
			{			
				Item item = (Item) Item.itemRegistry.getObject(stack.stackTagCompound.getString("itemName"));

				if (item != null) 
				{
					player.inventory.consumeInventoryItem(item);
					player.inventory.consumeInventoryItem(stack.getItem());
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public void onUpdate(ItemStack stack, World world, Entity entity, int par4, boolean par5)
	{
		if (stack.stackTagCompound == null) 
		{
			stack.stackTagCompound = new NBTTagCompound();
		}
	}

	@Override
	public void onCreated(ItemStack stack, World world, EntityPlayer player)
	{
	}

	@Override
	public void onPlayerStoppedUsing(ItemStack stack, World world, EntityPlayer player, int par4)
	{  	
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
	{
		if (par1ItemStack.stackTagCompound != null && par1ItemStack.getTagCompound().hasKey("itemName")) 
		{
			par3List.add("Subject: " + par1ItemStack.stackTagCompound.getString("itemName").toUpperCase());
		}
		if (TwilightZone.displayEpisodes == true)
		{
			par3List.add("EPISODE: Season 1, Episode 36");
			par3List.add(" NAME: A World of His Own");
		}
	}

	@Override
	public boolean canItemEditBlocks()
	{
		return false;
	}

	@Override
	public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ)
	{
		return false;
	}
}
