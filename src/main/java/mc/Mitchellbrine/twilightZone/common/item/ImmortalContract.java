package mc.Mitchellbrine.twilightZone.common.item;

import java.util.List;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

public class ImmortalContract extends Item
{
	public ImmortalContract() 
	{
	}

	@Override
	//called when the item is used (right click) return true if something happens, false if nothing happens (if something happens the animation will trigger)
	public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int par7, float par8, float par9, float par10)
	{
		if (stack.stackTagCompound == null) 
		{
			stack.stackTagCompound = new NBTTagCompound();
			stack.stackTagCompound.setString("owner", player.getCommandSenderName());
			return true;
		}
		else
		{
			if (player.isSneaking()) 
			{
				player.inventory.consumeInventoryItem(TwilightZone.immortalityContract);
				DamageSource contract = new DamageSource("contract").setDamageAllowedInCreativeMode().setDamageBypassesArmor();
				player.attackEntityFrom(contract, 1000);
				return true;
			}
		}
		return false;
	}
	/**
	 * Called each tick as long the item is on a player inventory. Uses by maps to check if is on a player hand and
	 * update it's contents.
	 */
	@Override
	public void onUpdate(ItemStack stack, World world, Entity entity, int par4, boolean par5)
	{
		if (entity instanceof EntityPlayer) 
		{
			EntityPlayer player = (EntityPlayer) entity;
			if (stack.stackTagCompound != null) 
			{
				String owner = stack.stackTagCompound.getString("owner");
				if (owner.equals(player.getCommandSenderName())) 
				{
					if (player.getHealth() <= 1.0F) 
					{
						player.setHealth(20.0F);
					}
				}
			}
		}
	}
	/**
	 * Called when item is crafted/smelted. Used only by maps so far.
	 */
	@Override
	public void onCreated(ItemStack stack, World world, EntityPlayer player)
	{
	}
	/**
	 * called when the player releases the use item button. Args: itemstack, world, entityplayer, itemInUseCount
	 */
	@Override
	public void onPlayerStoppedUsing(ItemStack stack, World world, EntityPlayer player, int itemInUseCount)
	{

	}
	/**
	 * allows items to add custom lines of information to the mouseover description
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List lore, boolean par4)
	{
		if (stack.stackTagCompound != null) 
		{
			String owner = stack.stackTagCompound.getString("owner");
			lore.add("This contract is binded to " + owner + "!");
		}
		
		if (TwilightZone.displayEpisodes == true) 
		{
			lore.add("EPISODE: Season 1, Episode 6");
			lore.add(" NAME: Escape Clause");
		}
	}
	/**
	 * Returns true if players can use this item to affect the world (e.g. placing blocks, placing ender eyes in portal)
	 * when not in creative
	 */
	@Override
	public boolean canItemEditBlocks()
	{
		return false;

	}
	/**
	 * This is called when the item is used, before the block is activated.
	 * @param stack The Item Stack
	 * @param player The Player that used the item
	 * @param world The Current World
	 * @param x Target X Position
	 * @param y Target Y Position
	 * @param z Target Z Position
	 * @param side The side of the target hit
	 * @return Return true to prevent any further processing.
	 */
	@Override
	public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) 
	{
		return false;
	}

}
