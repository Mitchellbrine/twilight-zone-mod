package mc.Mitchellbrine.twilightZone.common.item;

import java.util.List;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class GregorysRecorder extends Item
{
	public GregorysRecorder()
	{
		this.setMaxDamage(11);
		this.setMaxStackSize(1);
	}

	@Override
	public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int par7, float par8, float par9, float par10)
	{
		if (!world.isRemote) 
		{
			boolean isOn;

			if (stack.stackTagCompound != null) 
			{
				isOn = stack.stackTagCompound.getBoolean("isOn");
			}
			else 
			{
				isOn = false;
			}

			if (isOn == false) 
			{
				player.addChatMessage(new ChatComponentText("*Recorder turns on*"));
				stack.stackTagCompound.setBoolean("isOn", true);
			}
			else 
			{
				player.addChatMessage(new ChatComponentText("*Recorder turns off*"));
				stack.stackTagCompound.setBoolean("isOn", false);
			}
			
			return true;
		}
		return false;
	}

	@Override
	public void onUpdate(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5)
	{
		if (par1ItemStack.stackTagCompound != null) 
		{
			if (par1ItemStack.stackTagCompound.getBoolean("isOn") == false) 
			{
				par1ItemStack.setItemDamage(10);
			}
			else
			{
				par1ItemStack.setItemDamage(1);
			}
		}
		else 
		{
			par1ItemStack.stackTagCompound = new NBTTagCompound();
		}
	}

	@Override
	public void onCreated(ItemStack stack, World world, EntityPlayer player)
	{

	}

	@Override
	public void onPlayerStoppedUsing(ItemStack stack, World world, EntityPlayer player, int timeUsed)
	{

	}

	@SuppressWarnings("unchecked")
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, @SuppressWarnings("rawtypes") List lore, boolean par4)
	{
		if (stack.getItemDamage() == 1)
		{
			lore.add(EnumChatFormatting.DARK_RED + "� Recording");
		}
		if (TwilightZone.displayEpisodes == true)
		{
			lore.add("EPISODE: Season 1, Episode 36");
			lore.add(" NAME: A World of His Own");
		}
	}

	@Override
	public boolean canItemEditBlocks()
	{
		return false;
	}

	@Override
	public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) 
	{
		return false;
	}

}
