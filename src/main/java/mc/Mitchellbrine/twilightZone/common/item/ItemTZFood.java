package mc.Mitchellbrine.twilightZone.common.item;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;

public class ItemTZFood extends ItemFood{

	public ItemTZFood(int p_i45339_1_, float p_i45339_2_, boolean p_i45339_3_) {
		super(p_i45339_1_, p_i45339_2_, p_i45339_3_);
	}
	
    /**
     * How long it takes to use or consume an item
     */
    public int getMaxItemUseDuration(ItemStack par1ItemStack)
    {
        return 32;
    }

    /**
     * returns the action that specifies what animation to play when the items is being used
     */
    public EnumAction getItemUseAction(ItemStack par1ItemStack)
    {
    	if (par1ItemStack.getItem() == TwilightZone.fateBottle) {
    		return EnumAction.drink;
    	}
        return EnumAction.eat;
    }

}
