package mc.Mitchellbrine.twilightZone.common.block;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import mc.Mitchellbrine.twilightZone.client.sound.SoundHandling;
import mc.Mitchellbrine.twilightZone.common.tileentity.TileEntityDoorLocked;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class DoorLocked extends BlockContainer
{
	public DoorLocked() 
	{
		super(Material.wood);
		this.setBlockBounds(0.0F,0.0F,0.5625F,1.0F,2.0F,0.5F);
	}

	@Override
	public TileEntity createTileEntity(World par1World, int metadata) 
	{
		return new TileEntityDoorLocked();
	}

	@Override
	public TileEntity createNewTileEntity(World world, int par2)
	{
		return new TileEntityDoorLocked();
	}

	@Override
	public int getRenderType()
	{
		return -1;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	public boolean renderAsNormalBlock() 
	{
		return false;
	}


	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) 
	{
		if (player.getCurrentEquippedItem() != null && player.getCurrentEquippedItem().getItem() == TwilightZone.keyOfImagination)
		{
			if (!player.worldObj.isRemote)
			{
				player.addStat(TwilightZone.keyAch, 1);
				SoundHandling.onEntityPlayerVanilla("random.door_open", world, player, 1.0F, 1.0F);
				world.setBlock(x, y, z, TwilightZone.twilightZoneDoor);

			}
		}
		else 
		{
			SoundHandling.onEntityPlayerVanilla("random.door_close", world, player, 1.0F, 1.0F);
		}
		return false;
	}

	@Override
	public void onBlockDestroyedByPlayer(World world, int x, int y, int z, int oldMetadata) 
	{
		if (BlockUtilities.isNotBedrock(world, x, y, z))
		{
			world.setBlockToAir(x + 1, y - 1, z + 1);
			world.setBlockToAir(x + 1, y - 1, z - 1);
			world.setBlockToAir(x - 1, y - 1, z + 1);
			world.setBlockToAir(x - 1, y - 1, z - 1);
			world.setBlockToAir(x + 1, y - 1, z);
			world.setBlockToAir(x - 1, y - 1, z);
			world.setBlockToAir(x, y - 1, z + 1);
			world.setBlockToAir(x, y - 1, z - 1);
			world.setBlockToAir(x, y - 1, z);
		}
	}

	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack)
	{
		if (!world.isRemote)
		{
			if (entity instanceof EntityPlayer) 
			{
				EntityPlayer player = (EntityPlayer) entity;
				EntityPlayerMP playerMP = (EntityPlayerMP) player;
				if (!playerMP.func_147099_x().hasAchievementUnlocked(TwilightZone.twilightZone)) 
				{
					player.addStat(TwilightZone.twilightZone, 1);
					SoundHandling.onEntityPlay("tZIntro", world, entity, 20, 1);
				}
			}
			if (BlockUtilities.isNotBedrock(world, x, y, z)) 
			{
				world.setBlock(x + 1, y - 1, z + 1, TwilightZone.voidOfReality);
				world.setBlock(x + 1, y - 1, z - 1, TwilightZone.voidOfReality);
				world.setBlock(x - 1, y - 1, z + 1, TwilightZone.voidOfReality);
				world.setBlock(x - 1, y - 1, z - 1, TwilightZone.voidOfReality);
				world.setBlock(x + 1, y - 1, z, TwilightZone.voidOfReality);
				world.setBlock(x - 1, y - 1, z, TwilightZone.voidOfReality);
				world.setBlock(x, y - 1, z + 1, TwilightZone.voidOfReality);
				world.setBlock(x, y - 1, z - 1, TwilightZone.voidOfReality);
				world.setBlock(x, y - 1, z, TwilightZone.voidOfReality);
			}
			else 
			{
				world.setBlockToAir(x, y, z);
				EntityItem key = new EntityItem(world, x, y, z, new ItemStack(TwilightZone.twilightZoneDoor));
				key.setEntityItemStack(new ItemStack(TwilightZone.twilightZoneDoor, 1));
				key.setLocationAndAngles(x, y, z, 0, 0);
				world.spawnEntityInWorld(key);
			}
		}
		else 
		{
			if (entity instanceof EntityPlayer)
			{
				EntityPlayer player = (EntityPlayer) entity;
				EntityClientPlayerMP playerMP = (EntityClientPlayerMP) player;
				if (!playerMP.getStatFileWriter().hasAchievementUnlocked(TwilightZone.twilightZone)) 
				{
					player.addStat(TwilightZone.twilightZone, 1);
					//					SoundHandling.onEntityPlay("tZIntro", par1World, par5EntityLivingBase, 20, 1);
				}
			}	
		}
	}

	@Override
	public int onBlockPlaced(World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int metadata) 
	{
		BlockUtilities.setVoid(world, x, y, z);
		return metadata;
	}

}
