package mc.Mitchellbrine.twilightZone.common.block;

import java.util.Random;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import mc.Mitchellbrine.twilightZone.client.sound.SoundHandling;
import mc.Mitchellbrine.twilightZone.common.dimension.TZTeleporter;
import mc.Mitchellbrine.twilightZone.common.tileentity.TileEntityDoor;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class TwilightZoneDoor extends BlockContainer
{
	public TwilightZoneDoor()
	{
		super(Material.wood);
		this.setBlockBounds(0.0F,0.0F,0.5625F,1.0F,2.0F,0.5F);
	}

	@Override
	public TileEntity createTileEntity(World par1World, int metadata) 
	{
		return new TileEntityDoor();
	}

	@Override
	public TileEntity createNewTileEntity(World world, int par2)
	{
		return new TileEntityDoor();
	}

	@Override
	public int getRenderType()
	{
		return -1;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	public boolean renderAsNormalBlock() 
	{
		return false;
	}

	@Override
	public void onBlockDestroyedByPlayer(World world, int x, int y, int z, int oldMetadata) 
	{
		if (BlockUtilities.isNotBedrock(world, x, y, z))
		{
			world.setBlockToAir(x + 1, y - 1, z + 1);
			world.setBlockToAir(x + 1, y - 1, z - 1);
			world.setBlockToAir(x - 1, y - 1, z + 1);
			world.setBlockToAir(x - 1, y - 1, z - 1);
			world.setBlockToAir(x + 1, y - 1, z);
			world.setBlockToAir(x - 1, y - 1, z);
			world.setBlockToAir(x, y - 1, z + 1);
			world.setBlockToAir(x, y - 1, z - 1);
			world.setBlockToAir(x, y - 1, z);
		}
	}
	
	@Override
    public Item getItemDropped(int p_149650_1_, Random rand, int meta)
    {
            return Item.getItemFromBlock(TwilightZone.doorLocked);
    }
	
	@Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
		if (entity instanceof EntityPlayerMP) {
			
			EntityPlayerMP thePlayer = (EntityPlayerMP)entity;
			
			if (thePlayer.dimension != TwilightZone.twilightDimensionID)
			{
				thePlayer.mcServer.getConfigurationManager().transferPlayerToDimension(thePlayer, TwilightZone.twilightDimensionID, new TZTeleporter(thePlayer.mcServer.worldServerForDimension(TwilightZone.twilightDimensionID)));
				SoundHandling.onEntityPlay("tZTheme", world, entity, 1.0F, 1.0F);
			}
			else
			{
				thePlayer.mcServer.getConfigurationManager().transferPlayerToDimension(thePlayer, 0, new TZTeleporter(thePlayer.mcServer.worldServerForDimension(0)));
			}
		}
	}

}
