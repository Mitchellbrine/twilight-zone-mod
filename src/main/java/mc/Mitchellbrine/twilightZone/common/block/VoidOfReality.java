package mc.Mitchellbrine.twilightZone.common.block;

import mc.Mitchellbrine.twilightZone.common.entity.EntityAlicia;
import mc.Mitchellbrine.twilightZone.common.entity.EntityArchie;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class VoidOfReality extends Block
{
	public VoidOfReality() 
	{
		super(Material.dragonEgg);
	}

	@Override
	public void onEntityWalking(World par1World, int par2, int par3, int par4, Entity par5Entity) 
	{
		if (par5Entity instanceof EntityPlayer) 
		{
			EntityPlayer player = (EntityPlayer) par5Entity;
			player.addPotionEffect(new PotionEffect(Potion.blindness.id, 60, 0, true));
			player.addPotionEffect(new PotionEffect(Potion.confusion.id, 60, 0, true));
		}
		else if (!(par5Entity instanceof EntityAlicia) && (!(par5Entity instanceof EntityArchie)))
		{
			par5Entity.travelToDimension(1);
		}
	}

	//	@Override
	//	public int onBlockPlaced(World par1World, int par2, int par3, int par4, int par5, float par6, float par7, float par8, int par9) {
	//		Block leftHide = par1World.getBlock(par2 - 1, par3, par4);
	//		Block rightHide = par1World.getBlock(par2 + 1, par3, par4);
	//		Block forwardHide = par1World.getBlock(par2, par3, par4 + 1);
	//		Block backHide = par1World.getBlock(par2, par3, par4 - 1);
	//		
	//		if (leftHide.isAir(par1World, par2, par3, par4)) {
	//			if (rightHide.isAir(par1World, par2, par3, par4)) {
	//				if (forwardHide.isAir(par1World, par2, par3, par4)) {
	//					if (backHide.isAir(par1World, par2, par3, par4)) {
	//						Exception e = new Exception();
	//						e.printStackTrace();
	//					}
	//					else {
	//						this.setBlockTextureName(backHide.getBlockTextureFromSide(par5).toString());
	//					}
	//				}
	//				else {
	//					this.setBlockTextureName(forwardHide.getBlockTextureFromSide(par5).toString());	
	//				}
	//			}
	//			else {
	//				this.setBlockTextureName(rightHide.getBlockTextureFromSide(par5).toString());
	//			}
	//		}
	//		else {
	//			this.setBlockTextureName(leftHide.getBlockTextureFromSide(par5).toString());
	//		}
	//		return par9;
	//	}

}
