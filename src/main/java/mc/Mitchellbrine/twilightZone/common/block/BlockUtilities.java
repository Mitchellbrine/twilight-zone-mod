package mc.Mitchellbrine.twilightZone.common.block;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class BlockUtilities 
{
    public static boolean isNotBedrock(World world, int x, int y, int z)
    {
    	return !(world.getBlock(x, y - 1, z) == Blocks.bedrock || world.getBlock(x + 1, y - 1, z + 1) == Blocks.bedrock || world.getBlock(x + 1, y - 1, z - 1) == Blocks.bedrock || world.getBlock(x - 1, y - 1, z + 1) == Blocks.bedrock || world.getBlock(x - 1, y - 1, z - 1) == Blocks.bedrock || world.getBlock(x + 1, y - 1, z) == Blocks.bedrock || world.getBlock(x - 1, y - 1, z) == Blocks.bedrock || world.getBlock(x, y - 1, z + 1) == Blocks.bedrock || world.getBlock(x, y - 1, z - 1) == Blocks.bedrock); 
    }
    
    public static void setVoid(World world, int x, int y, int z) 
    {
		if (BlockUtilities.isNotBedrock(world, x, y, z)) 
		{
			world.setBlock(x + 1, y - 1, z + 1, TwilightZone.voidOfReality);
			world.setBlock(x + 1, y - 1, z - 1, TwilightZone.voidOfReality);
			world.setBlock(x - 1, y - 1, z + 1, TwilightZone.voidOfReality);
			world.setBlock(x - 1, y - 1, z - 1, TwilightZone.voidOfReality);
			world.setBlock(x + 1, y - 1, z, TwilightZone.voidOfReality);
			world.setBlock(x - 1, y - 1, z, TwilightZone.voidOfReality);
			world.setBlock(x, y - 1, z + 1, TwilightZone.voidOfReality);
			world.setBlock(x, y - 1, z - 1, TwilightZone.voidOfReality);
			world.setBlock(x, y - 1, z, TwilightZone.voidOfReality);
		}
    }
}
