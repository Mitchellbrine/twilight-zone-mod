package mc.Mitchellbrine.twilightZone.common.event;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class JoinChecker
{

	@SubscribeEvent
	public void playerJoinCheck(EntityJoinWorldEvent event) 
	{
		if (event.entity instanceof EntityPlayer)
		{
			if (!event.world.isRemote)
			{
				EntityPlayer player = (EntityPlayer)event.entity;
				if (TwilightZone.updateAvailable == true && TwilightZone.notifyMe == true) 
				{
					player.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_GRAY + "["+ EnumChatFormatting.WHITE + "The Twilight Zone Mod" + EnumChatFormatting.DARK_GRAY + "] " + EnumChatFormatting.WHITE + "New build available!"));
				}
			}
		}
	}

}
