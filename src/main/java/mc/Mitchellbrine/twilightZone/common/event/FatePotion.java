package mc.Mitchellbrine.twilightZone.common.event;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import mc.Mitchellbrine.twilightZone.client.sound.SoundHandling;
import mc.Mitchellbrine.twilightZone.common.item.RecorderHandling;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.ArrowLooseEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class FatePotion
{
	@SideOnly(Side.CLIENT)
	public int used;

	@SubscribeEvent
	public void onEntityUpdate(LivingUpdateEvent event) 
	{
		if (event.entityLiving.isPotionActive(TwilightZone.fatePotion)) 
		{
			if (event.entityLiving.getActivePotionEffect(TwilightZone.fatePotion).getDuration() == 0)
			{
				SoundHandling.onEntityPlayerVanilla("random.pop", event.entityLiving.worldObj, event.entityLiving, 1.0F, 1.0F);
				event.entityLiving.removePotionEffect(TwilightZone.fatePotion.id);
				return;
			}
		}
	}

	@SubscribeEvent
	public void chatEvent(ServerChatEvent event) 
	{
		RecorderHandling.calculateChatMessage(event.player, event.message, event.username);
	}

	@SubscribeEvent
	public void arrowShot(ArrowLooseEvent event) 
	{
		if (event.entityPlayer.isPotionActive(TwilightZone.fatePotion)) 
		{
			if (event.bow.getItem() == Items.bow) 
			{
				int damage = event.bow.getItemDamage() + 1;
				NBTTagCompound compound = event.bow.getTagCompound();

				event.entityPlayer.setCurrentItemOrArmor(0, new ItemStack(TwilightZone.fateBow, 1, damage));

				event.entityPlayer.getCurrentEquippedItem().setItemDamage(damage);
				event.entityPlayer.getCurrentEquippedItem().setTagCompound(compound);
			}
		}
		else 
		{
			if (event.bow.getItem() == TwilightZone.fateBow)
			{
				int damage = event.bow.getItemDamage() + 1;
				NBTTagCompound compound = event.bow.getTagCompound();

				event.entityPlayer.setCurrentItemOrArmor(0, new ItemStack(Items.bow, 1, damage));

				event.entityPlayer.getCurrentEquippedItem().setItemDamage(damage);
				event.entityPlayer.getCurrentEquippedItem().setTagCompound(compound);
			}			
		}

	}

}
