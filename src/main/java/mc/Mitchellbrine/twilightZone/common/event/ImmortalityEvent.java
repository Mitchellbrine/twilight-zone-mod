package mc.Mitchellbrine.twilightZone.common.event;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class ImmortalityEvent {

	@SubscribeEvent
	public void deathStopper(LivingHurtEvent event)
	{
		if (event.entityLiving instanceof EntityPlayer) 
		{
			EntityPlayer player = (EntityPlayer) event.entityLiving;
			if (player.inventory.hasItem(TwilightZone.immortalityContract)) 
			{
				ItemStack contract = new ItemStack(TwilightZone.immortalityContract);
				if (contract.stackTagCompound != null)
				{
					if (contract.stackTagCompound.hasKey("owner"))
					{
						if (contract.stackTagCompound.getString("owner") == player.getDisplayName()) 
						{
							if (event.ammount > player.getHealth()) 
							{
								event.setCanceled(true);
							}
						}
					}
					else if (!(contract.stackTagCompound.hasKey("owner")))
					{
						System.err.println("Test Failed!");
					}
				}

			}
		}
	}

}
