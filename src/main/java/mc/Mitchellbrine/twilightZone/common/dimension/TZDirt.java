package mc.Mitchellbrine.twilightZone.common.dimension;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDirt;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TZDirt extends BlockDirt
{
    public static final String[] types = new String[] {"default", "default", "podzol"};
    @SideOnly(Side.CLIENT)
    private IIcon podzol_top;
    @SideOnly(Side.CLIENT)
    private IIcon podzol_side;

    protected TZDirt()
    {
        super();
        
    }

    /**
     * Gets the block's texture. Args: side, meta
     */
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int p_149691_1_, int side)
    {
        if (side == 2)
        {
            if (p_149691_1_ == 1)
            {
                return this.podzol_top;
            }

            if (p_149691_1_ != 0)
            {
                return this.podzol_side;
            }
        }

        return this.blockIcon;
    }

    /**
     * Determines the damage on the item the block drops. Used in cloth and wood.
     */
    public int damageDropped(int metadata)
    {
        return 0;
    }

    @SideOnly(Side.CLIENT)
    public IIcon getIcon(IBlockAccess blockaccess, int x, int y, int z, int side)
    {
        int i1 = blockaccess.getBlockMetadata(x, y, z);

        if (i1 == 2)
        {
            if (side == 1)
            {
                return this.podzol_top;
            }

            if (side != 0)
            {
                Material material = blockaccess.getBlock(x, y + 1, z).getMaterial();

                if (material == Material.snow || material == Material.craftedSnow)
                {
                    return Blocks.grass.getIcon(blockaccess, x, y, z, side);
                }

                Block block = blockaccess.getBlock(x, y + 1, z);

                if (block != Blocks.dirt && block != Blocks.grass)
                {
                    return this.podzol_side;
                }
            }
        }

        return this.blockIcon;
    }

    /**
     * Returns an item stack containing a single instance of the current block type. 'i' is the block's subtype/damage
     * and is ignored for blocks which do not support subtypes. Blocks which cannot be harvested should return null.
     */
    protected ItemStack createStackedBlock(int metadata)
    {
        if (metadata == 1)
        {
        	metadata = 0;
        }

        return super.createStackedBlock(metadata);
    }

    /**
     * returns a list of blocks with the same ID, but different meta (eg: wood returns 4 blocks)
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
    public void getSubBlocks(Item item, CreativeTabs creativeTab, List items)
    {
        items.add(new ItemStack(this, 1, 0));
        items.add(new ItemStack(this, 1, 2));
    }

    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconregister)
    {
        super.registerBlockIcons(iconregister);
        this.podzol_top = iconregister.registerIcon(this.getTextureName() + "_" + "podzol_top");
        this.podzol_side = iconregister.registerIcon(this.getTextureName() + "_" + "podzol_side");
    }

    /**
     * Get the block's damage value (for use with pick block).
     */
    public int getDamageValue(World p_149643_1_, int p_149643_2_, int p_149643_3_, int p_149643_4_)
    {
        int l = p_149643_1_.getBlockMetadata(p_149643_2_, p_149643_3_, p_149643_4_);

        if (l == 1)
        {
            l = 0;
        }

        return l;
    }

}
