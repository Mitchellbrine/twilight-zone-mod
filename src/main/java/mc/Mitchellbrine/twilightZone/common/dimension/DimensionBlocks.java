package mc.Mitchellbrine.twilightZone.common.dimension;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import cpw.mods.fml.common.registry.GameRegistry;

public class DimensionBlocks
{
	public static DimensionTab dimensionTab;

	public static Block grass;
	public static Block dirt;

	public static void init()
	{
		dimensionTab = new DimensionTab(CreativeTabs.getNextID(),"twilightZoneDimension");

		grass = new TZGrass().setCreativeTab(dimensionTab).setStepSound(Block.soundTypeGrass).setHardness(0.6F).setBlockName("grass").setBlockTextureName("twilightZone:dimension/grass");
		dirt = new TZDirt().setCreativeTab(dimensionTab).setStepSound(Block.soundTypeGrass).setHardness(0.5F).setBlockName("dirt").setBlockTextureName("twilightZone:dimension/dirt");

		GameRegistry.registerBlock(grass, "grass");
		GameRegistry.registerBlock(dirt, "dirt");
	}

}