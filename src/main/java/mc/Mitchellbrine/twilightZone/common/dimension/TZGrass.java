package mc.Mitchellbrine.twilightZone.common.dimension;
 
import java.util.Random;
 
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
 
public class TZGrass extends Block
{
        @SideOnly(Side.CLIENT)
        private IIcon top;
        @SideOnly(Side.CLIENT)
        private IIcon sideSnow;
        @SideOnly(Side.CLIENT)
        private IIcon sideOverlay;
 
        public TZGrass()
        {
                super(Material.grass);
                this.setTickRandomly(true);
        }
 
        /**
         * Gets the block's texture. Args: side, meta
         */
        @SideOnly(Side.CLIENT)
        public IIcon getIcon(int side, int meta)
        {
                return side == 1 ? this.top : (side == 0 ? DimensionBlocks.dirt.getBlockTextureFromSide(side) : this.blockIcon);
        }
 
        /**
         * Ticks the block if it's been scheduled
         */
        public void updateTick(World world, int x, int y, int z, Random rand)
        {
                if (!world.isRemote)
                {
                        if (world.getBlockLightValue(x, y + 1, z) < 4 && world.getBlockLightOpacity(x, y + 1, z) > 2)
                        {
                                world.setBlock(x, y, z, Blocks.dirt);
                        }
                        else if (world.getBlockLightValue(x, y + 1, z) >= 9)
                        {
                                for (int l = 0; l < 4; ++l)
                                {
                                        int randX = x + rand.nextInt(3) - 1;
                                        int randY = y + rand.nextInt(5) - 3;
                                        int randZ = z + rand.nextInt(3) - 1;
 
                                        if (world.getBlock(randX, randY, randZ) == Blocks.dirt && world.getBlockMetadata(randX, randY, randZ) == 0 && world.getBlockLightValue(randX, randY + 1, randZ) >= 4 && world.getBlockLightOpacity(randX, randY + 1, randZ) <= 2)
                                        {
                                                world.setBlock(randX, randY, randZ, Blocks.grass);
                                        }
                                }
                        }
                }
        }
 
        public Item getItemDropped(int p_149650_1_, Random rand, int meta)
        {
                return Blocks.dirt.getItemDropped(0, rand, meta);
        }
 
        public boolean func_149851_a(World world, int x, int y, int z, boolean p_149851_5_)
        {
                return true;
        }
 
        public boolean func_149852_a(World world, Random rand, int x, int y, int z)
        {
                return true;
        }
 
        @SideOnly(Side.CLIENT)
        public IIcon getIcon(IBlockAccess world, int x, int y, int z, int side)
        {
                if (side == 1)
                {
                        return this.top;
                }
                else if (side == 0)
                {
                        return Blocks.dirt.getBlockTextureFromSide(side);
                }
                else
                {
                        Material material = world.getBlock(x, y + 1, z).getMaterial();
                        return material != Material.snow && material != Material.craftedSnow ? this.blockIcon : this.sideSnow;
                }
        }
 
        @SideOnly(Side.CLIENT)
        public void registerBlockIcons(IIconRegister iconReg)
        {
                this.blockIcon = iconReg.registerIcon(this.getTextureName() + "_side");
                this.top = iconReg.registerIcon(this.getTextureName() + "_top");
                this.sideSnow = iconReg.registerIcon(this.getTextureName() + "_side_snowed");
                this.sideOverlay = iconReg.registerIcon(this.getTextureName() + "_side_overlay");
        }
 
        public void func_149853_b(World world, Random rand, int x, int y, int z)
        {
                int int1 = 0;
 
                while (int1 < 128)
                {
                        int genX = x;
                        int genY = y + 1;
                        int genZ = z;
                        int int2 = 0;
 
                        while (true)
                        {
                                if (int2 < int1 / 16)
                                {
                                        genX += rand.nextInt(3) - 1;
                                        genY += (rand.nextInt(3) - 1) * rand.nextInt(3) / 2;
                                        genZ += rand.nextInt(3) - 1;
 
                                        if (world.getBlock(genX, genY - 1, genZ) == Blocks.grass && !world.getBlock(genX, genY, genZ).isNormalCube())
                                        {
                                                ++int2;
                                                continue;
                                        }
                                }
                                else if (world.getBlock(genX, genY, genZ).getMaterial() == Material.air)
                                {
                                        if (rand.nextInt(8) != 0)
                                        {
                                                if (Blocks.tallgrass.canBlockStay(world, genX, genY, genZ))
                                                {
                                                        world.setBlock(genX, genY, genZ, Blocks.tallgrass, 1, 3);
                                                }
                                        }
                                        else
                                        {
                                                world.getBiomeGenForCoords(genX, genZ).plantFlower(world, rand, genX, genY, genZ);
                                        }
                                }
 
                                ++int1;
                                break;
                        }
                }
        }
}