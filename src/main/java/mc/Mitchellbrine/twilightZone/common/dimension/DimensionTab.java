package mc.Mitchellbrine.twilightZone.common.dimension;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class DimensionTab extends CreativeTabs
{
    public DimensionTab(int index, String name)
    {
        super(index, name);
    }

    @SideOnly(Side.CLIENT)
 @Override
 public Item getTabIconItem()
    {
     return Item.getItemFromBlock(DimensionBlocks.grass);
    } 
}