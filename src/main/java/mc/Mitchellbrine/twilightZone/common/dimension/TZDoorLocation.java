package mc.Mitchellbrine.twilightZone.common.dimension;

import net.minecraft.util.ChunkCoordinates;

public class TZDoorLocation extends ChunkCoordinates
{
    public long lastUpdateTime;

    final TZTeleporter teleporter;

    public TZDoorLocation(TZTeleporter teleporter, int x, int y, int z, long lastUpdateTime)
    {
        super(x, y, z);
        this.teleporter = teleporter;
        this.lastUpdateTime = lastUpdateTime;
    }
}
