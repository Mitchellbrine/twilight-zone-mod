package mc.Mitchellbrine.twilightZone.common.dimension;

import mc.Mitchellbrine.twilightZone.common.entity.EntityArchie;
import net.minecraft.world.biome.BiomeGenBase;

public class TZBiome extends BiomeGenBase
{
    @SuppressWarnings("unchecked")
	public TZBiome(int par1)
    {
        super(par1);
        this.spawnableMonsterList.clear();
        this.spawnableWaterCreatureList.clear();
        this.theBiomeDecorator.treesPerChunk = 2;
        this.topBlock = DimensionBlocks.grass;
        this.fillerBlock = DimensionBlocks.dirt;
        setColor(244);
        this.spawnableCreatureList.clear();
        this.spawnableWaterCreatureList.clear();
        this.spawnableCaveCreatureList.clear();
        this.spawnableMonsterList.clear();
        
        this.spawnableMonsterList.add(new SpawnListEntry(EntityArchie.class, 10, 1, 2));
    }
    
//    public WorldGenAbstractTree func_150567_a(Random random)
//    {
//    	TODO Add trees here
//        return (WorldGenAbstractTree)(random.nextInt(10) == 0 ? TeleportationMod.worldGeneratorTeleTree : (random.nextInt(2) == 0 ? new WorldGenTeleportTrees(true, true) : (random.nextInt(3) == 0 ? new WorldGenTeleportTrees(true, false) : new WorldGenTeleportTrees(true, true))));
//    }
}
