package mc.Mitchellbrine.twilightZone;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

final class TwilightTab extends CreativeTabs{

    TwilightTab(int par1, String par2Str)
    {
        super(par1, par2Str);
    }

    @SideOnly(Side.CLIENT)

	@Override
	public Item getTabIconItem() {
    	return Item.getItemFromBlock(Blocks.dragon_egg);
    }	
	
}
