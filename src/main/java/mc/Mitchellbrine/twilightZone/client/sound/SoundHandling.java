package mc.Mitchellbrine.twilightZone.client.sound;

import net.minecraft.entity.Entity;
import net.minecraft.world.World;

public class SoundHandling 
{

	public static void onEntityPlay(String name, World world, Entity entity, float volume, float pitch) 
	{
		world.playSoundAtEntity(entity, "twilightZone:"+ name, (float) volume, (float) pitch);
	}
	
	public static void onEntityPlayerVanilla(String name, World world, Entity entity, float volume, float pitch)
	{
		world.playSoundAtEntity(entity, name, (float) volume, (float) pitch);		
	}
	
}
