package mc.Mitchellbrine.twilightZone.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelDoor extends ModelBase
{
	//fields
	ModelRenderer frameTop;
	ModelRenderer frameRight;
	ModelRenderer frameLeft;
	ModelRenderer door;
	ModelRenderer frameBottom;

	public ModelDoor()
	{
		textureWidth = 64;
		textureHeight = 64;

		frameTop = new ModelRenderer(this, 0, 38);
		frameTop.addBox(0F, 0F, 0F, 14, 1, 3);
		frameTop.setRotationPoint(-7F, -10F, -1F);
		frameTop.setTextureSize(64, 64);
		frameTop.mirror = true;
		setRotation(frameTop, 0F, 0F, 0F);
		frameRight = new ModelRenderer(this, 46, 0);
		frameRight.addBox(0F, 0F, 0F, 1, 33, 3);
		frameRight.setRotationPoint(7F, -10F, -1F);
		frameRight.setTextureSize(64, 64);
		frameRight.mirror = true;
		setRotation(frameRight, 0F, 0F, 0F);
		frameLeft = new ModelRenderer(this, 38, 0);
		frameLeft.addBox(0F, 0F, 0F, 1, 33, 3);
		frameLeft.setRotationPoint(-8F, -10F, -1F);
		frameLeft.setTextureSize(64, 64);
		frameLeft.mirror = true;
		setRotation(frameLeft, 0F, 0F, 0F);
		door = new ModelRenderer(this, 0, 0);
		door.addBox(0F, 0F, 0F, 14, 32, 1);
		door.setRotationPoint(-7F, -9F, 0F);
		door.setTextureSize(64, 64);
		door.mirror = true;
		setRotation(door, 0F, 0F, 0F);
		frameBottom = new ModelRenderer(this, 0, 34);
		frameBottom.addBox(0F, 0F, 0F, 16, 1, 3);
		frameBottom.setRotationPoint(-8F, 23F, -1F);
		frameBottom.setTextureSize(64, 64);
		frameBottom.mirror = true;
		setRotation(frameBottom, 0F, 0F, 0F);
	}

	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5);
		frameTop.render(f5);
		frameRight.render(f5);
		frameLeft.render(f5);
		door.render(f5);
		frameBottom.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5)
	{
		//super.setRotationAngles(f, f1, f2, f3, f4, f5);
	}

}
