package mc.Mitchellbrine.twilightZone.client;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import mc.Mitchellbrine.twilightZone.client.handler.DoorHandler;
import mc.Mitchellbrine.twilightZone.client.handler.DoorHandlerLocked;
import mc.Mitchellbrine.twilightZone.client.model.ModelTZBiped;
import mc.Mitchellbrine.twilightZone.client.render.RenderAlicia;
import mc.Mitchellbrine.twilightZone.client.render.RenderArchie;
import mc.Mitchellbrine.twilightZone.client.render.RenderItemBow;
import mc.Mitchellbrine.twilightZone.common.CommonProxy;
import mc.Mitchellbrine.twilightZone.common.entity.EntityAlicia;
import mc.Mitchellbrine.twilightZone.common.entity.EntityArchie;
import mc.Mitchellbrine.twilightZone.common.tileentity.TileEntityDoor;
import mc.Mitchellbrine.twilightZone.common.tileentity.TileEntityDoorLocked;
import net.minecraft.client.model.ModelBiped;
import net.minecraftforge.client.MinecraftForgeClient;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ClientProxy extends CommonProxy
{
	@Override
	public void registerThings()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityDoor.class, new DoorHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityDoorLocked.class, new DoorHandlerLocked());

		RenderingRegistry.registerEntityRenderingHandler(EntityAlicia.class, new RenderAlicia(new ModelBiped(),0.3F));
		RenderingRegistry.registerEntityRenderingHandler(EntityArchie.class, new RenderArchie(new ModelBiped(),0.3F));

		MinecraftForgeClient.registerItemRenderer(TwilightZone.fateBow, new RenderItemBow());
	}

}
