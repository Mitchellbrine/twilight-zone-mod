package mc.Mitchellbrine.twilightZone.client.render;

import mc.Mitchellbrine.twilightZone.TwilightZone;
import mc.Mitchellbrine.twilightZone.client.model.ModelTZBiped;
import mc.Mitchellbrine.twilightZone.common.entity.EntityArchie;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class RenderArchie extends RenderBiped
{
	protected ModelBiped model;
	private static ResourceLocation steveTextures = new ResourceLocation("twilightZone:textures/skins/arch.png");
	private static ResourceLocation zombieTextures = new ResourceLocation("twilightZone:textures/skins/zombie.png");
	private static ResourceLocation skeletonTextures = new ResourceLocation("twilightZone:textures/skins/skeleton.png");
	private static ResourceLocation creeperTextures = new ResourceLocation("twilightZone:textures/skins/creeper.png");
	private static ResourceLocation witherSkeletonTextures = new ResourceLocation("twilightZone:textures/skins/wither_skeleton.png");


	public RenderArchie(ModelBiped modelBiped, float f)
	{
		super(modelBiped, f);
		this.model = (ModelBiped) this.mainModel;
	}

	public void renderAlicia(EntityArchie entity, double x, double y, double z, float yaw, float partialRenderTicks)
	{
		super.doRender(entity, x, y, z, yaw, partialRenderTicks);
	}

	public void doRenderLiving(EntityLiving living, double x, double y, double z, float yaw, float partialRenderTicks)
	{
		this.renderAlicia((EntityArchie)living, x, y, z, yaw, partialRenderTicks);
	}

	public void doRender(Entity entity, double x, double y, double z, float yaw, float partialRenderTicks)
	{
		this.renderAlicia((EntityArchie)entity, x, y, z, yaw, partialRenderTicks);
	}

	/**
	 * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
	 */
	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		ResourceLocation resourcelocation = AbstractClientPlayer.locationStevePng;

		if (((EntityArchie)entity).playerName != null && ((EntityArchie)entity).playerName.length() > 0)
		{
			if (!((EntityArchie)entity).isArchieMHF() && !((EntityArchie)entity).isArchieVanilla())
			{
				resourcelocation = AbstractClientPlayer.getLocationSkin(((EntityArchie)entity).playerName);
				AbstractClientPlayer.getDownloadImageSkin(resourcelocation, ((EntityArchie)entity).playerName);
			}
			else
			{
				resourcelocation = AbstractClientPlayer.getLocationSkin("MHF_" + ((EntityArchie)entity).playerName);
				AbstractClientPlayer.getDownloadImageSkin(resourcelocation, "MHF_" + ((EntityArchie)entity).playerName);        		
				if (((EntityArchie)entity).isArchieVanilla()) {
					if (((EntityArchie)entity).getCustomNameTag().equalsIgnoreCase("Zombie")) 
					{
						return zombieTextures;
					}
					else if (((EntityArchie)entity).getCustomNameTag().equalsIgnoreCase("Skeleton"))
					{
						return skeletonTextures;
					}
					else if (((EntityArchie)entity).getCustomNameTag().equalsIgnoreCase("WitherSkeleton")) 
					{
						return witherSkeletonTextures;
					}
					else if (((EntityArchie)entity).getCustomNameTag().equalsIgnoreCase("Creeper"))
					{
						return creeperTextures;
					}
				}

			}
		}
		else
		{
			return steveTextures;
		}
		return resourcelocation;
	}

	@Override
	protected void rotateCorpse(EntityLivingBase entity, float x, float y, float z)
	{
		GL11.glRotatef(180.0F - y, 0.0F, 1.0F, 0.0F);

		if (entity.deathTime > 0)
		{
			float f3 = ((float)entity.deathTime + z - 1.0F) / 20.0F * 1.6F;
			f3 = MathHelper.sqrt_float(f3);

			if (f3 > 1.0F)
			{
				f3 = 1.0F;
			}

			GL11.glRotatef(f3 * this.getDeathMaxRotation(entity), 0.0F, 0.0F, 1.0F);
		}
		else
		{
			String s = EnumChatFormatting.getTextWithoutFormattingCodes(entity.getCommandSenderName());

			if ((s.equals("Dinnerbone") && !TwilightZone.playerBlacklist.contains("Dinnerbone")) || (s.equals("Grumm") && !TwilightZone.playerBlacklist.contains("Grumm")) && (!(entity instanceof EntityPlayer) || !((EntityPlayer)entity).getHideCape()))
			{
				GL11.glTranslatef(0.0F, entity.height + 0.1F, 0.0F);
				GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
			}
		}
	}
}

