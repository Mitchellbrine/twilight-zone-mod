package mc.Mitchellbrine.twilightZone.client.render;

import mc.Mitchellbrine.twilightZone.client.model.ModelTZBiped;
import mc.Mitchellbrine.twilightZone.common.entity.EntityAlicia;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.ResourceLocation;

public class RenderAlicia extends RenderLiving
{
	protected ModelBiped model;
	private static final ResourceLocation mobTexture = new ResourceLocation("twilightZone:entity/alicia.png");

	public RenderAlicia(ModelBiped modelBiped, float f)
	{
		super(modelBiped, f);
		this.model = (ModelBiped)this.mainModel;
	}

	public void renderAlicia(EntityAlicia entity, double x, double y, double z, float yaw, float partialRenderTicks)
	{
		super.doRender(entity, x, y, z, yaw, partialRenderTicks);
	}

	public void doRenderLiving(EntityLiving living, double x, double y, double z, float yaw, float partialRenderTicks)
	{
		this.renderAlicia((EntityAlicia)living, x, y, z, yaw, partialRenderTicks);
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method, always casting down its argument and then
	 * handing it off to a worker function which does the actual work. In all probabilty, the class Render is generic
	 * (Render<T extends Entity) and this method has signature public void doRender(T entity, double d, double d1,
	 * double d2, float f, float f1). But JAD is pre 1.5 so doesn't do that.
	 */
	public void doRender(Entity entity, double x, double y, double z, float yaw, float partialRenderTicks)
	{
		this.renderAlicia((EntityAlicia)entity, x, y, z, yaw, partialRenderTicks);
	}

	/**
	 * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
	 */
	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return mobTexture;
	}
}

