package mc.Mitchellbrine.twilightZone.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import mc.Mitchellbrine.twilightZone.Constants;
import mc.Mitchellbrine.twilightZone.TwilightZone;

public class AutoUpdateChecker
{
	public static boolean isUpdateAvailable() throws IOException, MalformedURLException 
	{
		BufferedReader versionFile = new BufferedReader(new InputStreamReader(new URL("https://dl.dropboxusercontent.com/u/133619815/tZoneVersion.txt").openStream()));
		String curVersion = versionFile.readLine();

		versionFile.close();
		if (curVersion.contains(Constants.VERSION))
		{
			return true;
		}
		else
		{
			TwilightZone.updateAvailable = true;
		}

		return false;
	}	
}
