![](http://i.imgur.com/GX9kkxx.png)

- - -

"You unlock this door with the key of imagination. Beyond it is another dimension - a dimension of sound, a dimension of sight, a dimension of mind. You're moving into a land of both shadow and substance, of things and ideas. You've just crossed over into the Twilight Zone."
-Rod Serling


In this mod, that land comes to life in "blocky" form. Enter the door and into... the Twilight Zone!